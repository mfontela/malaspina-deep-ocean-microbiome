---
title: Deep ocean metagenomes provide insight into the metabolic architecture of bathypelagic microbial communities
date: 2021-04-28
---
Silvia G. Acinas, Pablo Sánchez, Guillem Salazar, Francisco M. Cornejo-Castillo, Marta Sebastián, Ramiro Logares, Marta Royo-Llonch, Lucas Paoli, Shinichi Sunagawa, Pascal Hingamp, Hiroyuki Ogata, Gipsi Lima-Mendez, Simon Roux, José M. González, Jesús M. Arrieta, Intikhab S. Alam,  Allan Kamau, Chris Bowler, Jeroen Raes, Stéphane Pesant, Peer Bork, Susana Agustí, Takashi Gojobori, Dolors Vaqué, Matthew B. Sullivan, Carlos Pedrós-Alió, Ramon Massana, Carlos M. Duarte, Josep M. Gasol. _Communications Biology_ 4, 604 (2021). [doi:10.1038/s42003-021-02112-2](https://doi.org/10.1038/s42003-021-02112-2)    

## Companion website data to the manuscript  

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/malaspina-jgi-deep-stations-map-labelled.png)
<p style="margin-left: 80px"><font size="-0.5">World map showing 32 stations from the Malaspina Expedition 2010.</font></p>


### Malaspina Gene Database (M-geneDB)

A total of 4,039,279 predicted coding sequences from the assembled metagenomes were pooled, and sequences shorter than 100 bp were filtered out to render a final 3,872,410 redundant sequences dataset. This dataset was clustered at 95% of sequence similarity and 90% of sequence overlap of the smaller sequence using cd-hit-est (v.4.6; Li and Godzik 2006) using the following options:

```
 -c 0.95 -T 0 -M 0 -G 0 -aS 0.9 -g 1 -r 1 -d 0 
```

to obtain 1,115,269 non-redundant gene clusters (from now on referred simply as genes).

Such catalog (Malaspina Gene Database; _M-geneDB_) can be downloaded here:  

- [Deep M-geneDB gene collection](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/catalog/M-geneDB.v1_1115269-seqs.fasta.gz) [Size: 141M | MD5 sum: 1150667356f3dc0b2a9d7eddac91cd91]   

With the full header annotation available here:  

- [Deep M-geneDB gene collection annotation](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/catalog/M-geneDB.v1_1115269-seqs_full-header-annotation.txt.gz) [Size: 21M | MD5 sum: 8e4bc701bf167c20505f6ddee42676cb]   

Full sequence headers hold an ordinal identifier and the annotation information as follows:

```
M-geneDB.v1_000000261_MP0103_JGI11754J13217_10000457_COG1404_PF00082,PF04829_ko:K17734_""_k_Bacteria;p_Proteobacteria;c_Gammaproteobacteria;o_Alteromonadales;f_Alteromonadaceae;g_Alteromonas;s_Alteromonas sp. KUL17
```  


Where the different annotation items are separated by underscores `_` except the *seed sequence identifier* that has 2 undersores in it as discussed below:  
- `M-geneDB.v1_000000261`: M-geneDB unique identifier  
- `MP0103_JGI11754J13217_10000457`: seed sequence identifier. This is the sequence that started a particular cluster and holds three pieces of information separated by 2 underscores: 1) Malaspina sample where the sequence belongs, 2) JGI identifier of the sample analysis, 3) JGI locus identifier.  
- `COG1404`: COG annotation of that sequence (NCBI CDD database, COG v1.0). If more than one COG is associated to the gene they will be sepparated by commas.  
- `PF00082,PF04829`: PFAM annotation of that sequence (PFAM database release 28.0). Same as above.  
- `ko:K17734`: KEGG's ko annotation of that sequence (KEGG database Release 2015-10-12). Same as above.  
- `md:####`: KEGG's module annotation if available.  
- `_k_Bacteria;p_Proteobacteria;c_Gammaproteobacteria;o_Alteromonadales;f_Alteromonadaceae;g_Alteromonas;s_Alteromonas sp. KUL17` homology search against `UniRef100 Release 2019-10-16`.   

Empty cells are denoted by "" (two double-quotes).  


In order to explore the novelty of the M-geneDB, we clustered it with the 40,154,822 non-redundant sequences from the [Tara Oceans Microbial Reference Gene Catalog v2 (OM-RGC.v2; Salazar et al. 2019)](https://doi.org/10.1016/j.cell.2019.10.014) using cd-hit-est-2d (v.4.6; Li and Godzik 2006) using the following options: 

```
-c 0.95 -T 24 -M 74000 -G 0 -aS 0.9 -g 1 -r 1 -d 0 
```

The subset of 647,817 novel M-geneDB genes appended to the OM-RGC.v2 yields a final catalog of 47,422,971 genes. Novel Malaspina genes can be downloaded here:  

- [Novel M-geneDB genes](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/catalog/M-geneDB.v1_647817-novel-genes.fasta.gz) [Size: 83M | MD5 sum: 574057f60446f7d64e99405ca5fa7037]   


***

### Functional tables
Functional Tables (ie. COGs, Pfam and Enzyme) were provided by [IMG](https://img.jgi.doe.gov/cgi-bin/m/main.cgi) [Compare Genomes/Abundance Profiles/Overview (All functions)] using the "*estimated gene copies*" option (i.e. Estimated by multiplying by read depth when available instead of using raw gene count). So the numbers in the tables correspond to the number of reads (i.e. read counts) for a given function in a given sample.

Subsampled tables have been constructed by randomly sampling every sample reads to a given sample size with "*rrarefy*" function in *vegan* package (R Statistical computing Software). The chosen sample size is the minimum number of reads in a sample. The random rarefaction is made without replacement so that the variance of rarefied communities is rather related to rarefaction proportion than to to the size of the sample.

Minimum number of gene estimated copies (equivalent to reads) in a sample (i.e. subsampling size used):

```
 - COGs    --> 219,444   
 - PFAMs   --> 342,607  
 - ENZYMEs --> 86,681  
 - KO      --> 12,078  
```

#### Raw Functional Tables

Raw functional tables can be downloaded here:  

- [COG table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/raw/Table_COG.txt?inline=false) [Size: 919K | MD5 sum: 4044aaa5b41e1a8e3f9da5e5d679c9d2]  
- [Enzyme (EC) table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/raw/Table_ENZYME.txt?inline=false) [Size: 457K | MD5 sum: 47d641bc072302b526ee66fc14a4f325]  
- [KEGG orthology table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/raw/Table_KO.txt?inline=false) [Size: 1.1M | MD5 sum: 4882c4c4ec9bc9df189d818a657a095a]  
- [PFAM table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/raw/Table_PFAM.txt?inline=false) [Size: 2.5M | MD5 sum: eeff7970244da03688d6405dacf48920]  


#### Subsampled Functional Tables

Functional tables subsampled as described above can be downloaded here:  
 
- [COG table subsampled](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/subsampled/Table_COG.ss.txt?inline=false) [Size: 778K | MD5 sum: dd3a2881ea8b620d54d0f9dc5176b80d]  
- [Enzyme (EC) table subsampled](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/subsampled/Table_ENZYME.ss.txt?inline=false) [Size: 384K | MD5 sum: cd05b5fe8172ab1c80d339d06cf5f4b2]  
- [KEGG orthology table subsampled](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/subsampled/Table_KO.ss.txt?inline=false) [Size: 702K | MD5 sum: 9173c129bf503eed2bdb4e569c1e1c7c]  
- [PFAM table subsampled](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/functional-tables/subsampled/Table_PFAM.ss.txt?inline=false) [Size: 2.1M | MD5 sum: 146846226b9db2b4ee249dc6889a01c8]  

***

### Taxonomic tables

Taxonomic Tables were provided by [IMG](https://img.jgi.doe.gov/cgi-bin/m/main.cgi) [Compare Genomes/Phylogenetic Dist./Metagenomes vs. Genomes] using the "estimated gene copies" option (i.e. Estimated by multiplying by read depth when available instead of using raw gene count) and the 60+ Perc. Identity option. A Phylum-level table was downloaded for all samples. For Proteobacteria, Class-level table was also downloaded and merged, composing a table with all Phyla and Proteobacteria divided into their Classes.  

- [Raw abundance taxonomy table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/taxonomic-tables/Table_TAXONOMY.txt?inline=false) [Size: 14K | MD5 sum: 70c15e7b595ebfff0b1fa2354106e532]  

Estimated gene copies for each sample were divided by the total copies in order to obtain relative abundances and correct for different sampling depth. This table is the one to be used (unles a specific reason exists for using the raw table).  

- [Relative abundance taxonomy table](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/taxonomic-tables/Table_TAXONOMY.rel.txt?inline=false) [Size: 38K | MD5 sum: b9367ac05d22252bf263b4bb5c0da92f]  

***

### Metagenome Assembled Genomes (MAGs)  

To build the Malaspina Deep Metagenome Assembled Genomes (MAGs) catalogue (MDeep-MAGs), all 58 metagenomes from the Malaspina expedition were pooled and co-assembled (megahit v1.2.8; Li et al., 2016; options: `--presets meta-large --min-contig-len 2000`). Resultant contigs were de-replicated with cd-hit-est (v4.8.1 compiled for long sequence support; `MAX_SEQ=1000000`, with options `-c 0.95 -n 10 -G 0 -aS 0.95 -d`). With this procedure we increased the sequence space and we obtained a total of 421,891 contigs larger than 2,000 bp. Metagenomic reads were back-mapped to the contigs dataset (bowtie v2.3.4.1 with default options; Langmead and Salzberg 2012), keeping only mapping hits with quality larger than 10 (samtools v1.8; Li et al., 2009). We then binned the contigs into a total of 619 bins according to differential coverage and tetranucleotide frequencies in metabat (v2.12.1; Kang et al., 2015; `jgi_summarize_bam_contig_depths` and metabat with default options). A second round of assembly was carried out within each bin with CAP3 (v2015-10-02; Huang and Madan 1999; options `-o 16 -p 95 -h 100 -f`) to solve overlapping overhangs with 95% of sequence similarity between contigs.   

The catalog of MAGs with estimated genome completeness larger than 50% and less than 10% contamination can be downloaded from:  

- [317 MAGs fasta sequences](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/mags/fasta/acinas-et-al-2020_Malaspina-deep-HQ-MQ-317-MAG-set.tar.gz?inline=false) [Size: 276M | MD5 sum: 753e553c0bbdf32f791e1ee09bf6ae22]  

The annotation of such MAGs (COG, PFAM and KEGG KOs) can be downloaded from:

- [317 MAGs annotation (GFF/tsv)](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/mags/annotation/acinas-et-al-2020_Malaspina-deep-HQ-MQ-317-MAG-set-annotation.tar.gz?inline=false) [Size: 49M | MD5 sum: 6473a686e19a7c9bc30825955178c48e]  

***

### Manuscript supplementary tables

**Supplementary Table 1. Description of the data availability**  
|Data type|Repository|  
|---|---|  
|Raw metagenomic data|European Nucleotide Archive [PRJEB44456](https://www.ebi.ac.uk/ena/browser/view/PRJEB44456) - Suppl. Information S1|
|M-GeneDB|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|
|Raw functional tables|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457) |
|Subsampled functional tables|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|
|Raw taxonomy abundance tables|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|
|Relative abundance taxonomy table|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|  
|Co-assembly of 58 metagenomes from the deep Ocean|European Nucleotide Archive [PRJEB40454](https://www.ebi.ac.uk/ena/browser/view/PRJEB40454)|
|Sequences of 317 MAGs of the global deep Ocean|BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|
|Functional annotation of 317 MAGs of the global deep Ocean|  BioStudies [S-BSST457](https://www.ebi.ac.uk/biostudies/studies/S-BSST457)|

***

***

### Manuscript supplementary information

- [Supplementary information](https://gitlab.com/malaspina-public/malaspina-deep-ocean-microbiome/raw/master/content/files/supplementary-tables/supplementary-data.xlsx?inline=false) [Size: 326K | MD5 sum: 14675fadbca804faeba9193361e8878b]  

***

### Manuscript supplementary figures

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig1.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 1</font></p>  

**Supplementary Figure 1**. Relative taxonomic composition at the domain level (Archaea, Bacteria and Eukarya plus Viruses) of the genes of the M-GeneDB found in each of the 58 bathypelagic metagenomes. FL, free-living size fraction (0.2-0.8μm); PA, particle-attached size fraction (0.8- 20μm).  
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig2.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 2</font></p>  

**Supplementary Figure 2.** Histograms showing those genes with a functional annotation based on KEGG metabolism hierarchy III (KO) within the novel genes of the Malaspina Gene DataBase (M-GeneDB). This represent 37% of the novel genes of this catalogue since the rest has no functional annotation and it is only shown for those genes with more than 5000 counts.  
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig3.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 3</font></p>  

**Supplementary Figure 2.** Relative abundance of protists, prokaryotes, giruses, and viruses in the bathypelagic ocean. A) Protists (small eukaryotes) only in the 0.8-20 μm, B) Prokaryotes (Bacteria and Archaea), C) Giruses and D) Viruses (prophage genes in metagenomes) from the Malaspina bathypelagic metagenomes. Different gene markers or strategies were used to assess the diversity and compute abundances from the metagenomes: i) for protist the 18S miTags approach was used, ii) for prokaryotes we used clade-specific marker genes from 3,000 reference genomes of Archaea and Bacteria to generate taxonomic abundance profiles in each sample, iii) the marker gene of the Nucleo-cytoplasmic Large DNA Viruses (NCLDV) major capsid was used for giruses and iv) the marker gene of the large subunit of the Terminase (TerL) was used for viruses. The left panel presents the relative abundance of each group per station split into the two size fractions (particle attached, PA, and Free-living, FL). The X-axis shows station (St) and oceans (Atlantic, Indian and Pacific). The right panel shows in pie charts the relative abundances of picoeukaryotes, prokaryotes, giruses and viruses as averages of all the stations.  
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig4.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 4</font></p>  

**Supplementary Figure 4.** Bray Curtis distances among sites using non-metric multidimensional scaling (NMDS) ordination plots based on the functional abundance tables constructed with A) Protein families (Pfams) and B) Enzyme Commission numbers (ECs) and C) Cluster of Orthologous groups (COGs). Note that in this figure the station 62 at 2400 m is included whereas it was excluded from Fig. 2.  
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig5.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 5</font></p>  

**Supplementary Figure 5.** Phylogenetic analyses of the 19 sequences related to K10944 (_pmoA_/_amoA_) and PF12942 (archaeal _amoA_) found within the 317 MAGs. These 19 sequences from our MAGs are labelled in yellow in the outer ring.  
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig6.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 6</font></p>  

**Supplementary Figure 6.** Phylogeny of Proteobacteria _nifH_ genes. Phylogenetic analysis of 3 potentially diazotroph MAGs from the bathypelagic ocean (red \*), including their Blast nr best hits (orange +), _nifH_ genes from Proteobacteria (NCBI-IPG) and _nifH_ sequences from Delmont et. al 2018 MAGs. Phylogenetic group I from Delmont et al., 2018 is shaded in blue.
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig7.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 7</font></p>  

**Supplementary Figure 7.** Maximum-Likelihood phylogenetic reconstruction of 18 MAGs containing the RuBisCo gene (_rbcL_; K01601) from the Malaspina MDeep-MAGs dataset. The phylogenic tree was done using the RuBisCo large-chain reference alignment profile published by Jaffe et al., 2019, together with the large-chain sequences from heterotrophic marine Thaurmarchaeota published by Aylward and Santoro, 2020.
<br/><br/>

![image](https://gitlab.com/malaspina-public/test/raw/master/content/page/supFig8.jpg)
<p style="margin-left: 80px"><font size="-0.5">Supplementary Figure 8</font></p>  

**Supplementary Figure 8.** Rank abundance curve presenting the accumulated abundance of the 317 MAGs based on reads per kilobase per genome equivalent (RPKGs) and coloring the 16 MAGs with identified genetic potential for chemolithoautotrophy. The MAG with chemolithoautotrophy potential are the Ammonia Oxidizing Archaea (AOA), Ammonia Oxidizing Bacteria (AOB), Nitrite Oxidizing Bacteria (NOB), Sulfur Oxidizing bacteria (SOB) and MAGs containing pathways for inorganic carbon fixation such as the 3-Hydroxypropianate (3-HP) or MAGs with the RuBisCo Forms I and II genes associated to autotrophy.
<br/><br/>


### References

- Aylward, F. O. & Santoro, A. E. Heterotrophic Thaumarchaeota with ultrasmall genomes are widespread in the ocean. bioRxiv 2020.03.17.996280 (2020). doi:10.1101/2020.03.17.996280  
- Delmont, T. O. et al. Nitrogen-fixing populations of Planctomycetes and Proteobacteria are abundant in surface ocean metagenomes. Nat. Microbiol. 3, 804–813 (2018)  
- Huang, X. & Madan, A. CAP3: A DNA Sequence Assembly Program Resource 868 Genome Research. Genome Res. 868–877 (1999).  
- Jaffe, A. L., Castelle, C. J., Dupont, C. L. & Banfield, J. F. Lateral Gene Transfer Shapes the Distribution of RuBisCO among Candidate Phyla Radiation Bacteria and DPANN Archaea. Mol. Biol. Evol. 36, 435–446 (2019).  
- Kang, D. D., Froula, J., Egan, R. & Wang, Z. MetaBAT, an efficient tool for accurately reconstructing single genomes from complex microbial communities. PeerJ 3, e1165 (2015)  
- Langmead, B. & Salzberg, S. L. Fast gapped-read alignment with Bowtie 2. Nat. Methods 9, 357–359 (2012)  
- Li, D. et al. MEGAHIT v1.0: A fast and scalable metagenome assembler driven by advanced methodologies and community practices. Methods 102, 3–11 (2016)  
- Li, H. et al. The Sequence Alignment/Map format and SAMtools. Bioinformatics 25, 2078–2079 (2009)  
- Li, W., Godzik, A. (2006). Cd-hit: a fast program for clustering and comparing large sets of protein or nucleotide sequences, Bioinformatics, Volume 22, Issue 13, Pages 1658–1659   
- Salazar, G. et al. Gene Expression Changes and Community Turnover Differentially Shape the Global Ocean Metatranscriptome. Cell 179, 1068-1083.e21 (2019).   


 
